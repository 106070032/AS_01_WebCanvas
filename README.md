# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

| **Item**                                         | **Score** |
| :----------------------------------------------: | :-------: |
| Basic components                                 | 60%       |
| Advance tools                                    | 35%       |
| Appearance (subjective)                          | 5%        |
| Other useful widgets (**describe on README.md**) | 1~10%     |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

---

## Put your report below here
Functions discribtion:
功能:
Basic :
	(1)Basic control tools
		a)Brush
			滑鼠點下後，等待移動，在每一個移動前移動後的座標之間使用，ctx.lineTo，
			在兩點間畫線，集結所有點變成一條滑鼠路徑的線
			
		b)Eraser
			將brush 顏色改為底色的白色，並把游標圖示改成橡皮擦
			
		c)Color Selector
			依據點擊的色塊不同，用strokeStyle調整畫出來的顏色及游標的顏色
			
		d)Brush size menu
			依據點擊的大小不同，用lineWidth調整線條粗細
	
	(2)Text Input
		將在下面text bar 輸入的文字，以char 儲存，新增text 框，並同時顯示在畫框的最頂
	
	(3)Cursor Icon
		在點擊不同的功能後，更改cursor.style，游標不同處有:
		a)預設:十字
		b)筆: 依照選擇的顏色，變更游標的顏色
		c)橡皮擦:顯示橡皮擦圖示
		d)畫圖型:十字
		e)頁面其他地區:預設箭頭
		
	(4)Refresh Button
		用clear rectangle 來清除整個畫布(undo redo 所用的紀錄變數也同步清空)
		
Advance:
	(1)Different Brush Shape
		拉圖型:
			使用兩層canvas，上層設為透明，在畫的過程中(mousemove)不斷刷新上層，
			再放開滑鼠後(mouseup)將上層匯入下層，以達到畫圖的效果。
		設座標:
			紀錄滑鼠初始點及目前位置的座標，並由兩點間距離及座標畫圓形三角形及矩形
		圖型屬性:
			會根據所選的顏色及粗度，改變所畫的圖型屬性
		
	(2)Un/Re Do Function
		使用array 將每次的作圖存檔，當undo時，畫上前一張圖，Redo則畫下一張圖
		
	(3)Image Tool
		upload local 端圖片，並顯示在滑鼠點擊處，在點及其他筆之前，都會顯示圖片再點擊處，
		類似蓋印章概念，點擊其他筆的按鈕，則會改成筆
		
	(4)Download
		將所畫好的圖的url 改成 myImage.png，在點擊按鈕之後下載作畫
		
Bonus :
	(1)漸層筆:
		會依據畫出來線的長度，做顏色漸層
	(2)彩色游標:
		每個顏色都有各個顏色的游標
	(3)印章概念:
		upload 上來的圖檔可以無限次剪貼
		
	
	
		
	
		
		
		
		
		




