var canvas = document.getElementById('art');
var tempCanvas = document.getElementById('image_temp');
var ctx = canvas.getContext("2d");
 
var tempContext = tempCanvas.getContext("2d");
var cPushArray = new Array();
var cStep = -1;
var radius=50;
var nStartX = 0;
var nStartY = 0;
var colors = ['red', 'blue', 'green', 'purple', 'yellow', 'orange', 'pink', 'black', 'white', 'ebebeb'];
var size = [1, 3, 5, 10, 15, 20];
var sizeNames = ['default', 'three', 'five', 'ten', 'fifteen', 'twenty'];
var pen = ['marker','fade','circle','square','triangle'];
var penNumber = 0;


var colorRGB = "0,0,0";
var colorName = "black";
var penName = "marker";
var prob = "1";
var img = new Image();
var img_load = false;
var bIsDrawing = false;


async function getMousePos(canvas, evt) {
  var rect = await canvas.getBoundingClientRect();
  return {
    x: evt.clientX - rect.left,
    y: evt.clientY - rect.top
  };
}

async function mouseMove(evt) {
  //var rect = await canvas.getBoundingClientRect();
  var mousePos = await getMousePos(canvas, evt);
  //if(mousePos.x>rect.left && mousePos.x<rect.right && mousePos.y>rect.top && mousePos < rect.bottom){
    ctx.lineTo(mousePos.x, mousePos.y);
    //console.log(mousePos.x
    ctx.stroke();
  //}
  
}

tempCanvas.addEventListener('mousedown', async function(evt) {
  console.log(penName);
  if(penName == "circle"){
    putPoint(evt);
    drawPoint(1,false,evt);
    tempCanvas.addEventListener('mousemove', mousemove1, false);
  }
  else if(penName == "square"){
    putPoint(evt);
    drawPoint(2,false,evt);
    tempCanvas.addEventListener('mousemove', mousemove2, false);
  }
  else if(penName=="triangle"){
    putPoint(evt);
    drawPoint(3,false,evt);
    tempCanvas.addEventListener('mousemove', mousemove3, false);
  }
  else{
    try{
      var mousePos = getMousePos(canvas, evt);
      ctx.beginPath();
      ctx.moveTo(mousePos.x, mousePos.y);
      evt.preventDefault();
      await tempCanvas.addEventListener('mousemove', mouseMove, false);
    }catch(e){
      console.log(e);
    }
  }
  

  
});
function mousemove1 (evt){
  drawPoint(1,false,evt);
}
function mousemove2 (evt){
  drawPoint(2,false,evt);
}
function mousemove3 (evt){
  drawPoint(3,false,evt);
}

tempCanvas.addEventListener('mouseup', async function(e) {
  if(penName == "circle"){
    drawPoint(1, true ,e);
    tempCanvas.removeEventListener('mousemove', mousemove1, false);
    stopPoint();
    cPush();
  }
  else if(penName == "square"){
    drawPoint(2, true ,e);
    tempCanvas.removeEventListener('mousemove', mousemove2, false);
    stopPoint();
    cPush();
  }
  else if(penName == "triangle"){
    tempCanvas.removeEventListener('mousemove', mousemove3, false);
    drawPoint(3, true ,e);
    stopPoint();
    cPush();
  }
  else{
    await tempCanvas.removeEventListener('mousemove', mouseMove, false);
    cPush();
    console.log(cStep);
  }

}, false);

document.getElementById('reset').addEventListener('click', function() {
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  tempContext.clearRect(0, 0, tempCanvas.width, tempCanvas.height);
  cStep=-1;
  cPushArray = [];
}, false);

/*
document.getElementById('download').addEventListener('click', function(el) {

  var image = canvas.toDataURL("image/jpg");
  el.href = image;
}, false);*/

download_img = function(el) {
  //document.getElementById('download').getAttribute('download') = "myImage.jpg";
  var image = canvas.toDataURL("image/jpg");
  el.href = image;
};

var putPoint = function(e){
  nStartX = e.clientX;
  nStartY = e.clientY;
  bIsDrawing = true;
  radius = 0;
}
var drawPoint = function(num,finish,e){
  console.log(finish);
  var context = (finish) ? ctx : tempContext;
  if(finish == true ){
    

      tempContext.clearRect(0, 0, canvas.width, canvas.height);
  }
  if(finish == false) {
    context.clearRect(0, 0, canvas.width, canvas.height);

  }

  if(!bIsDrawing)
    return;
  
  var endX = e.clientX ;
  var endY =e.clientY;
  if(num == 2){
    var width = Math.abs(endX - nStartX);
    var height = Math.abs(endY - nStartY);
    context.strokeStyle = ctx.strokeStyle;
    context.lineWidth = ctx.lineWidth;
    context.strokeRect(nStartX, nStartY, width, height);
  }
  else if(num ==1){
    var w = endX - nStartX;
    var h = endY - nStartY;
    var offsetX = (w < 0) ? w : 0;
    var offsetY = (h < 0) ? h : 0;
    var width = Math.abs(w);
    var height = Math.abs(h);
    var radius = Math.sqrt(Math.pow((nStartX - endX), 2) + Math.pow((nStartY - endY), 2));
    //tempContext.clearRect(0, 0, canvas.width, canvas.height);
    context.beginPath();
    context.arc(nStartX, nStartY, radius, 0, Math.PI*2);
    context.strokeStyle = ctx.strokeStyle;
    context.lineWidth = ctx.lineWidth;
    context.stroke();
  }
  else if(num == 3){

    context.beginPath();
        context.moveTo(nStartX,nStartY);
        context.lineTo(endX, endY);
        context.lineTo(-endX+2*nStartX, endY);
        context.strokeStyle = ctx.strokeStyle;
        context.lineWidth = ctx.lineWidth;
        context.closePath();
        context.stroke();
        

  }
  


  
}
var stopPoint = function(e){
  bIsDrawing = false;
  
}

//-------------------------------------------------------------------------------------------------
//=======================================================================================================
//=======================================================================================================
//=======================================================================================================
//=======================================================================================================
function listener(i) {
  var el = document.getElementById(colors[i]);
  if(el){
    el.addEventListener('click', async function() {
      img_load = false;
      colorRGB = document.getElementById(colors[i]).getAttribute('value');
      colorName = colors[i];
      //canvas.dragging {cursor: crosshair;}
  
      await change_cursor("url('"+colorName+"_crayon.png'),auto");
      //canvas.style.cursor = red;
      //$('selector').css({'cursor': 'url('+colors+'_crayon.png), default'});
      //console.log(colorRGB);
      //ctx.strokeStyle = rgb+"("+colorRGB+", 0.1)";
      if(colors[i]!="white"){
        ctx.strokeStyle = "rgba("+colorRGB+", "+prob+")";
      }else{
        ctx.strokeStyle = "rgba("+colorRGB+", 1)";
      }
      
      //console.log(colors[i]);
      //console.log(ctx.strokeStyle);
    }, false);
  }

}
function change_cursor(url){
  canvas.style.cursor = url;
  tempCanvas.style.cursor = url;
}


//-------------------------------------------------------------------------------------------------
async function cPush() {
  cStep++;
  if (cStep < cPushArray.length) { cPushArray.length = cStep; }
  await cPushArray.push(canvas.toDataURL());
  //console.log(canvas.toDataURL());
}
async function cUndo() {
  if (cStep >0) {
      cStep--;
      var canvasPic = new Image();
      canvasPic.src = cPushArray[cStep];
      //await ctx.drawImage(canvasPic, 0, 0);
      await ctx.clearRect(0, 0, canvas.width, canvas.height);
      canvasPic.onload =  function () { ctx.drawImage(canvasPic, 0, 0); }
      console.log (cStep);
  }
  else if(cStep ==0){
      //var canvasPic = new Image();
      //canvasPic.src = cPushArray[cStep];
      //await ctx.drawImage(canvasPic, 0, 0);
      await ctx.clearRect(0, 0, canvas.width, canvas.height);
      //canvasPic.onload =  function () { ctx.drawImage(canvasPic, 0, 0); }
      cStep--;
      console.log (cStep);
  }
}
function cRedo() {
  if (cStep < cPushArray.length-1) {
      cStep++;
      var canvasPic = new Image();
      canvasPic.src = cPushArray[cStep];
      canvasPic.onload = function () { ctx.drawImage(canvasPic, 0, 0); }
      console.log (cStep);
  }
}

//------------------------------------------------------------------------------------------------------------
var imageLoader = document.getElementById('imageLoader');
imageLoader.addEventListener('change', image_load_check, false);


function image_load_check(e){
  console.log(img_load);
  img_load = true;
  handleImage(e);
}

function handleImage(e){
  if(img_load==true){
    change_cursor("url('img_icon.png'),auto");
    var reader = new FileReader();
    reader.onload = function(event){
      //var img = new Image();
      img.onload = function(){
        
        tempCanvas.addEventListener('mousedown', async function(evt) {
          try{
            var mousePos = getMousePos(tempCanvas, evt);
            ctx.beginPath();
            ctx.moveTo(mousePos.x, mousePos.y);
            evt.preventDefault();
            await tempCanvas.addEventListener('mouseup', loadImg, false);
            console.log(img_load);
          }catch(e){
            console.log(e);
          }
          
        });
      }
      img.src = event.target.result;
    }
    reader.readAsDataURL(e.target.files[0]);     
  }
  
}

async function loadImg(evt){
  if(img_load ==true){
    var mousePos = await getMousePos(tempCanvas, evt);
    console.log(mousePos.x);

      ctx.drawImage(img,mousePos.x,mousePos.y);

  }
  
}


//-----------------------------------------------------------------------------------------------------




function fontSizes(i) {
  document.getElementById(sizeNames[i]).addEventListener('click', function() {
    //ctx.setLineWidth(size[i]);
    img_load = false;
    ctx.lineJoin = "round";
		ctx.lineWidth = size[i];
  }, false);
}
function penType(i) {
  document.getElementById(pen[i]).addEventListener('click', async function() {
    //await imageLoader.removeEventListener('change', handleImage, false);
    img_load = false;
    penNumber = i;
    //console.log(img_load);
    //ctx.setLineWidth(size[i]);
    penName = pen[i];
    console.log(penName);
    if(penName == "circle"||penName == "triangle"||penName == "square"){
      change_cursor("crosshair");
      ctx.strokeStyle = "rgba("+colorRGB+", 1)";
    }
    else{
      change_cursor("url('"+colorName+"_crayon.png'),auto");
    
      prob = document.getElementById(pen[i]).getAttribute('text');
      ctx.strokeStyle = "rgba("+colorRGB+", "+prob+")";
    }

    //console.log(prob);
  }, false);
}
for(var i = 0; i < pen.length; i++) {
  penType(i);
}

for(var i = 0; i < colors.length; i++) {
  listener(i);
}

for(var i = 0; i < size.length; i++) {
  fontSizes(i);
}


//-------------------------------------------------------------------------------------------------



function clearCanvas(cnv) {
  var ctx = canvas.getContext('2d');     // gets reference to canvas context
  ctx.beginPath();    // clear existing drawing paths
  ctx.save();         // store the current transformation matrix

  // Use the identity matrix while clearing the canvas
  ctx.setTransform(1, 0, 0, 1, 0, 0);
  ctx.clearRect(0, 0, cnv.width, cnv.height);

  ctx.restore();        // restore the transform
}



function  addTextCnv(ctx, text, x, y, maxWidth, lineHeight) {
  // splits the text in words to can wrap it on new lie if exceds maxWidth
  var words = text.split(' ');
  console.log(words);
  var nr_w = words.length
  var addtxt = '';

  // sets to add the text and rows
  for(var n = 0; n < nr_w; n++  ) {
    var txtLine = addtxt+words[n]+' ';
    var metrics = ctx.measureText(txtLine);
    var txtWidth = metrics.width;
    if (txtWidth > maxWidth && n > 0) {
      ctx.fillText(addtxt, x, y);
      addtxt = words[n]+' ';
      y  = lineHeight;
    }
    else addtxt = txtLine;
    console.log(txtLine);
  }

  // adds the text in canvas (sets text color, font type and size)
  ctx.fillStyle = '#0001be';
  ctx.font = 'bold 17px sans-serif';
  ctx.fillText(addtxt, x, y);
}

var maxWidth = canvas.width - 10;
var lineHeight = 23;
var x_pos = (canvas.width - maxWidth) / 2;
var y_pos = 15;

// register onkeyup event for #text_cnv text field to add the text in canvas as it is typed
document.getElementById('text_cnv').onkeyup = function() {
  clearCanvas(canvas);      // clears the canvas
  addTextCnv(ctx, this.value, x_pos, y_pos, maxWidth, lineHeight);
}




//-------------------------------------------------------------------------------------------------
/*		<label for = "imageLoader" >
<img src = "upload.png" top = -30 left = 1105/>
</label>*/